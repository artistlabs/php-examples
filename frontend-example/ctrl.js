/**
 * Created by vladimir on 23.10.2015.
 */
/**
 * Created with PhpStorm.
 * User: antonio<antonio.lightsoft@gmail.com>
 * Date: 06.02.2015
 * Time: 11:32
 */
(function (window) {
    'use strict';

    var _fn = function ($scope, commonFilterService, ajaxService, $sce, $modal) {
        this.commonFilterService = commonFilterService;
        this.ajaxService = ajaxService;
        this.scope = $scope;
        //this.scope.template;
        this.sce = $sce;
        this.modal = $modal;
    };

    _fn.prototype.init = function () {
        this.commonFilterService.getSavedFilterData(function (data) {
            this.ajaxService.getRequest({
                method: 'POST',
                data: {
                    filter: data
                },
                url: '/agency/confirm'
            }).then(function (data, status) {
                if (data.error) {
                    this.commonFilterService.errorPopup(data.error).then(null, function () {
                        if (data.redirect) {
                            location.href = data.redirect;
                        }
                    });
                }
                if(data.template) {
                    this.template = data.template;
                }
            }.bind(this), null);
        }.bind(this));
    };

    _fn.prototype.testShare = function() {
        var modalInstance = this.modal.open({
            templateUrl: 'modal_template_confirm_social_share.html',
            controller: ['$scope', '$modalInstance', 'app.common.ajax_service' , 'commonFilterService', 'messages', this.commonFilterService.shareModalController],
            resolve: {
                messages: function() {
                    return {
                        message_share_ok: 'share OK',
                        message_share_error: 'share ERROR'
                    };
                }
            }
        });
    };

    window.requestConfirmCtrl = _fn;
})(window);