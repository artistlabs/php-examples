/**
 * Created by vladimir on 23.10.2015.
 */

(function (window) {
    'use strict';
    var init = function(){

        var mod = angular.module('requestConfirmModule', [
            '720kb.socialshare'
        ]);

        mod.controller('ConfirmCtrl', ['$scope', 'commonFilterService', 'app.common.ajax_service', '$sce', '$modal', window.requestConfirmCtrl ]);

        return mod;

    };

    window.ngApp.app.requires = window.ngApp.app.requires.concat([ init().name ]);
    window.ngApp.start();

})(window);