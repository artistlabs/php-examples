<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 26.07.2017
 * Time: 18:31
 */

namespace AppBundle\system\provider;


use AppBundle\contracts\IExternalCurrencyProvider;
use AppBundle\services\IConfigManager;
use AppBundle\system\helpers\HtmlLoader;
use Symfony\Component\DomCrawler\Crawler;

class ExternalCurrencyProvider implements IExternalCurrencyProvider
{
    private $configManager;
    private $orginalCurrency;

    public function __construct(IConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    public function getCurrency(): float
    {
        return floatval($this->getOriginalCurrency() + $this->getCurrencyIncrement());
    }

    public function getCurrencyIncrement(): float
    {
        return 2.0;
    }

    public function getOriginalCurrency(): float
    {
        if($this->orginalCurrency === null) {
            $html = HtmlLoader::loadHtmlByUrl($this->configManager->getCurrencySyncUrl());
            $crawler = new Crawler();
            $crawler->addContent($html);
            $this->orginalCurrency = (float)$crawler->filter('body section.main_container div.col-course table.table_course tbody tr:nth-child(3) td:last-child')->text();
        }

        return $this->orginalCurrency;
    }
}