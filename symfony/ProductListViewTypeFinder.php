<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.07.2017
 * Time: 16:14
 */

namespace AppBundle\system;


use AppBundle\contracts\IProductListViewTypeResolver;
use AppBundle\system\vo\ProductListViewType;
use Webmozart\Assert\Assert;
use Zend\Stdlib\PriorityQueue;

class ProductListViewTypeFinder
{
    private $resolvers;

    public function __construct()
    {
        $this->resolvers = new PriorityQueue();
    }

    public function addResolver(IProductListViewTypeResolver $resolver, $priority = 0) {
        $this->resolvers->insert($resolver, $priority);
    }

    /**
     * @return ProductListViewType|null
     */
    public function getViewType() {
        foreach ($this->resolvers as $resolver) {
            /**
             * @var IProductListViewTypeResolver $resolver
             */
            $type = $resolver->getViewType();
            if($type) {
                Assert::isInstanceOf($type, ProductListViewType::class);
                return $type;
            }
        }

        return null;
    }

}