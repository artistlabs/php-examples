<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.07.2017
 * Time: 16:47
 */

namespace AppBundle\system;


use AppBundle\contracts\IProductListViewTypeContext;
use AppBundle\system\vo\ProductListViewType;
use Webmozart\Assert\Assert;

final class ProductListViewTypeContext implements IProductListViewTypeContext
{
    private $finder;
    private $viewType;

    public function __construct(ProductListViewTypeFinder $finder)
    {
        $this->finder = $finder;
    }

    public function getViewType()
    {
        if($this->viewType === null) {
            $this->viewType = $this->finder->getViewType();
            Assert::notNull($this->viewType, 'not set view type context');
            Assert::isInstanceOf($this->viewType, ProductListViewType::class);
        }
        return $this->viewType;
    }
}