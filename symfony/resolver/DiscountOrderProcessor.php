<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12.07.2017
 * Time: 18:09
 */

namespace AppBundle\system\resolver;


use AppBundle\services\IPriceCalcService;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;

class DiscountOrderProcessor implements OrderProcessorInterface
{
    private $calcService;
    public function __construct(IPriceCalcService $calcService)
    {
        $this->calcService = $calcService;
    }

    public function process(OrderInterface $order): void
    {
        $this->calcService->calculateDiscount($order);
    }
}