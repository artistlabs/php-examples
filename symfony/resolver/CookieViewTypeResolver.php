<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.07.2017
 * Time: 18:58
 */

namespace AppBundle\system\resolver;


use AppBundle\contracts\IProductListViewTypeResolver;
use AppBundle\system\vo\ProductListViewType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CookieViewTypeResolver implements IProductListViewTypeResolver
{
    private $request;

    public function __construct(RequestStack $request)
    {
        $this->request = $request->getCurrentRequest();
    }

    public function getViewType()
    {
        if($this->request->cookies->has('view_type')) {
            return new ProductListViewType($this->request->cookies->get('view_type'));
        }
        return null;
    }
}