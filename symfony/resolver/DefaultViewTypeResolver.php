<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.07.2017
 * Time: 17:50
 */

namespace AppBundle\system\resolver;

class DefaultViewTypeResolver implements \AppBundle\contracts\IProductListViewTypeResolver
{
    public function getViewType()
    {
        return new \AppBundle\system\vo\ProductListViewType(\AppBundle\system\vo\ProductListViewType::TYPE_LIST);
    }
}