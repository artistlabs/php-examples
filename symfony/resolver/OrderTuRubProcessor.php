<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 12.07.2017
 * Time: 18:37
 */

namespace AppBundle\system\resolver;


use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Currency\Context\CurrencyContextInterface;
use Sylius\Component\Currency\Converter\CurrencyConverterInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;

class OrderTuRubProcessor implements OrderProcessorInterface
{
    private $orderManager;
    private $currencyConverter;
    private $currencyContext;

    public function __construct(
        EntityManagerInterface $orderManager,
        CurrencyConverterInterface $currencyConverter,
        CurrencyContextInterface $currencyContext
    )
    {
        $this->orderManager = $orderManager;
        $this->currencyConverter = $currencyConverter;
        $this->currencyContext = $currencyContext;
    }


    public function process(OrderInterface $order): void
    {
        if($this->currencyContext->getCurrencyCode() === 'RUB') {
            foreach ($order->getItems() as $orderItem) {
                $rub_price = $this->currencyConverter->convert($orderItem->getUnitPrice(), 'USD', 'RUB');
                $orderItem->setUnitPrice((int)$rub_price);
                $order->addItem($orderItem);
            }
            $this->checkCartCurrency($order);

            $this->orderManager->persist($order);
            $this->orderManager->flush();
        }
    }

    private function checkCartCurrency(OrderInterface $order) {
        if($order->getCurrencyCode() !== $this->currencyContext->getCurrencyCode()) {
            $order->setCurrencyCode($this->currencyContext->getCurrencyCode());
        }
    }
}