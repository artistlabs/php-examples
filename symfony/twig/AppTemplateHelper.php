<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.06.2017
 * Time: 14:47
 */

namespace AppBundle\system\twig;


use AppBundle\contracts\IProductListViewTypeContext;
use AppBundle\services\IPriceCalcService;

class AppTemplateHelper
{
    private $calcService;
    private $viewTypeContext;

    public function __construct(IPriceCalcService $calcService, IProductListViewTypeContext $viewTypeContext)
    {
        $this->calcService = $calcService;
        $this->viewTypeContext = $viewTypeContext;
    }

    public function getWholesaleType() {
        return $this->calcService->getWholesaleType()->getCurrent();
    }

    public function getViewType() {
        return $this->viewTypeContext->getViewType()->getCurrentType();
    }
}