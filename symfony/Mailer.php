<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.07.2017
 * Time: 18:51
 */

namespace AppBundle\system;


class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    private $from;

    public function __construct(\Swift_Mailer $mailer, $from)
    {
        $this->mailer = $mailer;
        $this->from = $from;
    }

    /**
     * @param $to
     * @param $body
     * @return int
     */
    public function simpleSend($to, $body) {
        /**
         * @var \Swift_Message $message
         */
        $message = $this->mailer->createMessage();
        $message
            ->setFrom($this->from)
            ->setTo($to)
            ->setBody($body)
        ;

        return $this->mailer->send($message);
    }
}