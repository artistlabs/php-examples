<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 19.06.2017
 * Time: 17:19
 */

namespace AppBundle\system;


use AppBundle\services\IPriceCalcService;
use Sylius\Component\Core\Calculator\ProductVariantPriceCalculatorInterface;
use Sylius\Component\Core\Exception\MissingChannelConfigurationException;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Webmozart\Assert\Assert;

class PriceCalculator implements ProductVariantPriceCalculatorInterface
{
    private $calcService;

    public function __construct(IPriceCalcService $calcService)
    {
        $this->calcService = $calcService;
    }

    public function calculate(ProductVariantInterface $productVariant, array $context): int
    {
        Assert::keyExists($context, 'channel');

        $channelPricing = $productVariant->getChannelPricingForChannel($context['channel']);

        if (null === $channelPricing) {
            throw new MissingChannelConfigurationException(sprintf(
                'Channel %s has no price defined for product variant %s',
                $context['channel']->getName(),
                $productVariant->getName()
            ));
        }

        return $this->calcService->calcRetailPrice($channelPricing->getPrice(), $productVariant->getPriceRule());
    }
}