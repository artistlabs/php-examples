<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 26.07.2017
 * Time: 19:17
 */

namespace AppBundle\system\handlers;


class UpdateExchangeRateCommand
{
    private $baseCurrency;
    private $targetCurrency;
    private $rate;

    public function __construct(string $baseCurrency, string $targetCurrency, float $rate)
    {
        $this->baseValute = $baseCurrency;
        $this->targetValute = $targetCurrency;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->baseValute;
    }

    /**
     * @return string
     */
    public function getTargetCurrency(): string
    {
        return $this->targetValute;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }
}