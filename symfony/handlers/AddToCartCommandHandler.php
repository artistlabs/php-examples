<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.07.2017
 * Time: 15:21
 */

namespace AppBundle\system\handlers;


use AppBundle\Entity\Product;
use AppBundle\Entity\ProductVariant;
use Sylius\Bundle\OrderBundle\Doctrine\ORM\OrderItemRepository;
use Sylius\Component\Core\Model\OrderItem;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Repository\OrderRepositoryInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Core\Repository\ProductVariantRepositoryInterface;
use Sylius\Component\Inventory\Checker\AvailabilityChecker;
use Sylius\Component\Inventory\Checker\AvailabilityCheckerInterface;
use Sylius\Component\Order\Context\CartContextInterface;
use Sylius\Component\Order\Modifier\OrderItemQuantityModifierInterface;
use Sylius\Component\Order\Modifier\OrderModifierInterface;
use Sylius\Component\Order\Repository\OrderItemRepositoryInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Webmozart\Assert\Assert;

class AddToCartCommandHandler
{
    private $cartContext;
    private $orderItemFactory;
    private $productRepository;
    private $orderModifier;
    private $orderItemQuantityModifier;
    private $availabilityChecker;

    public function __construct(
        CartContextInterface $cartContext,
        FactoryInterface $orderItemFactory,
        ProductRepositoryInterface $productRepository,
        OrderModifierInterface $orderModifier,
        OrderItemQuantityModifierInterface $orderItemQuantityModifier,
        AvailabilityCheckerInterface $availabilityChecker
    )
    {
        $this->cartContext = $cartContext;
        $this->orderItemFactory = $orderItemFactory;
        $this->productRepository = $productRepository;
        $this->orderModifier = $orderModifier;
        $this->orderItemQuantityModifier = $orderItemQuantityModifier;
        $this->availabilityChecker = $availabilityChecker;
    }

    public function handle(AddToCartCommand $command) {
        $cart = $this->cartContext->getCart();

        /**
         * @var Product $product
         */
        $product = $this->productRepository->find($command->getProductId());

        $pv = $product->getVariants();
        if($pv->count() > 1) {
            throw new \InvalidArgumentException('product variants it`s more 1');
        }

        /**
         * @var OrderItem $order_item
         * @var ProductVariant $variant
         */
        $variant = $pv->first();

        $order_item = $this->orderItemFactory->createNew();
        $order_item->setVariant($pv->first());
        $old_item = $this->resolveOrderItem($order_item);

        $this->orderItemQuantityModifier->modify($order_item, $command->getQuantity());

        if($variant->isTracked()) {
            $count = $old_item? ($old_item->getQuantity() + $order_item->getQuantity()) : $order_item->getQuantity();
            Assert::true(
                $this->availabilityChecker->isStockSufficient($variant, $count),
                'На складе нет такого кол-ва товара, обратитесь к менеджерам за уточнением точного кол-ва');
        }

        $this->orderModifier->addToOrder($cart, $order_item);
    }

    /**
     * @param OrderItemInterface $item
     * @return mixed|\Sylius\Component\Order\Model\OrderItemInterface
     */
    private function resolveOrderItem(OrderItemInterface $item)
    {
        foreach ($this->cartContext->getCart()->getItems() as $existingItem) {
            if ($item->equals($existingItem)) {
                return $existingItem;
            }
        }
        return null;
    }
}