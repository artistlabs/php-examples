<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 03.07.2017
 * Time: 14:58
 */

namespace AppBundle\system\handlers;

use AppBundle\contracts\IAddToCartCommand;

class AddToCartCommand implements IAddToCartCommand
{
    private $productId;
    private $quantity;

    public function __construct($productId, $quantity = 1)
    {
        $this->productId = $productId;
        $this->quantity = $quantity;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }
}