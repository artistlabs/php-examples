<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 26.07.2017
 * Time: 19:21
 */

namespace AppBundle\system\handlers;


use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Currency\Model\ExchangeRate;
use Sylius\Component\Currency\Repository\ExchangeRateRepositoryInterface;
use Webmozart\Assert\Assert;

class UpdateExchangeRateCommandHandler
{
    private $exchangeRateRepository;
    private $manager;

    public function __construct(
        ExchangeRateRepositoryInterface $exchangeRateRepository,
        EntityManagerInterface $manager
    )
    {
        $this->manager = $manager;
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    public function handle(UpdateExchangeRateCommand $command)
    {
        $rate = $this->exchangeRateRepository->findOneWithCurrencyPair($command->getBaseCurrency(), $command->getTargetCurrency());
        Assert::isInstanceOf($rate, ExchangeRate::class);
        Assert::float($command->getRate());

        $rate->setRatio($command->getRate());

        $this->manager->persist($rate);
        $this->manager->flush();
    }
}