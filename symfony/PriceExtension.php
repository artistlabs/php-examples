<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.06.2017
 * Time: 16:29
 */

namespace AppBundle\system;


use AppBundle\services\IPriceCalcService;
use AppBundle\system\vo\WholesaleType;
use Sylius\Component\Core\Exception\MissingChannelConfigurationException;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Webmozart\Assert\Assert;

class PriceExtension extends \Twig_Extension
{
    private $calcService;

    public function __construct(IPriceCalcService $calcService)
    {
        $this->calcService = $calcService;
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('app_retail_price', [$this, 'calcRetailPrice']),
            new \Twig_SimpleFilter('app_wholesale_price_1', [$this, 'calcWholesalePrice1']),
            new \Twig_SimpleFilter('app_wholesale_price_2', [$this, 'calcWholesalePrice2']),
            new \Twig_SimpleFilter('app_wholesale_price_3', [$this, 'calcWholesalePrice3']),
            new \Twig_SimpleFilter('app_wholesale_price_4', [$this, 'calcWholesalePrice4']),
            new \Twig_SimpleFilter('app_wholesale_message', [$this, 'getWholesaleMessage']),
        ];
    }

    public function calcRetailPrice(ProductVariantInterface $productVariant, array $context) {
        $channelPricing = $this->getChannelPricing($productVariant, $context);
        return $this->calcService->calcRetailPrice($channelPricing->getPrice(), $productVariant->getPriceRule());
    }

    public function calcWholesalePrice1(ProductVariantInterface $productVariant, array $context) {
        $channel_pricing = $this->getChannelPricing($productVariant, $context);
        return $this->calcService->calcWholesalePrice1($channel_pricing->getPrice(), $productVariant->getPriceRule());
    }

    public function calcWholesalePrice2(ProductVariantInterface $productVariant, array $context) {
        $channel_pricing = $this->getChannelPricing($productVariant, $context);
        return $this->calcService->calcWholesalePrice2($channel_pricing->getPrice(), $productVariant->getPriceRule());
    }

    public function calcWholesalePrice3(ProductVariantInterface $productVariant, array $context) {
        $channel_pricing = $this->getChannelPricing($productVariant, $context);
        return $this->calcService->calcWholesalePrice3($channel_pricing->getPrice(), $productVariant->getPriceRule());
    }

    public function calcWholesalePrice4(ProductVariantInterface $productVariant, array $context) {
        $channel_pricing = $this->getChannelPricing($productVariant, $context);
        return $this->calcService->calcWholesalePrice4($channel_pricing->getPrice(), $productVariant->getPriceRule());
    }

    public function getWholesaleMessage($type) {
        switch ($type) {
            case WholesaleType::WHOLESALE_NONE:
                $message = 'Вы покупаете по розничной цене';
                break;
            case WholesaleType::WHOLESALE_1:
                $message = 'Сумма корзины превышает 5000 рублей. Вы получили скидку по мелкому опту';
                break;
            case WholesaleType::WHOLESALE_2:
                $message = 'Сумма корзины превышает 15000 рублей. Вы получили скидку по среднему опту';
                break;
            case WholesaleType::WHOLESALE_3:
                $message = 'Сумма корзины превышает 30000 рублей. Вы получили скидку по крупному опту';
                break;
            case WholesaleType::WHOLESALE_4:
                $message = 'Сумма корзины превышает 100000 рублей. Вы получили скидку по опт4';
                break;
            default:
                $message = '';

        }

        return $message;
    }


    /**
     * @param ProductVariantInterface $productVariant
     * @param array $context
     * @return null|\Sylius\Component\Core\Model\ChannelPricingInterface
     */
    private function getChannelPricing(ProductVariantInterface $productVariant, array $context) {
        Assert::keyExists($context, 'channel');

        $channelPricing = $productVariant->getChannelPricingForChannel($context['channel']);

        Assert::notNull($channelPricing, sprintf(
            'Channel %s has no price defined for product variant %s',
            $context['channel']->getName(),
            $productVariant->getName()
        ));

        return $channelPricing;
    }
}