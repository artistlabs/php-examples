<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.06.2017
 * Time: 17:18
 */

namespace AppBundle\system;


use Sylius\Bundle\MoneyBundle\Formatter\MoneyFormatterInterface;
use Webmozart\Assert\Assert;

/**
 * @author Łukasz Chruściel <lukasz.chrusciel@lakion.com>
 */
final class MoneyFormatter implements MoneyFormatterInterface
{
    /**
     * {@inheritdoc}
     */
    public function format(int $amount, string $currency, ?string $locale = 'en'): string
    {
        $formatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);

        $result = $formatter->formatCurrency(abs(round($amount / 100)), $currency);
        Assert::notSame(
            false,
            $result,
            sprintf('The amount "%s" of type %s cannot be formatted to currency "%s".', $amount, gettype($amount), $currency)
        );

        return $amount >= 0 ? $result : '-' . $result;
    }
}
