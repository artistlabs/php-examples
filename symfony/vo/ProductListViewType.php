<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.07.2017
 * Time: 16:15
 */

namespace AppBundle\system\vo;


use Webmozart\Assert\Assert;

class ProductListViewType
{
    const TYPE_LIST = 'list';
    const TYPE_GRID = 'grid';

    private $type;

    public function __construct($type)
    {
        Assert::oneOf($type, self::getTypes());

        $this->type = $type;
    }

    public function getCurrentType() {
        return $this->type;
    }

    public function isEqual($type) {
        Assert::oneOf($type, self::getTypes());

        return $this->type === $type;
    }

    public function setType($type) {
        return new self($type);
    }

    public static function getTypes() {
        return [self::TYPE_GRID, self::TYPE_LIST];
    }
}