<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.06.2017
 * Time: 13:45
 */
namespace AppBundle\system\vo;

use Webmozart\Assert\Assert;

class WholesaleType
{
    const WHOLESALE_NONE = 0;
    const WHOLESALE_1 = 1;
    const WHOLESALE_2 = 2;
    const WHOLESALE_3 = 3;
    const WHOLESALE_4 = 4;

    private $type;

    public function __construct(int $type)
    {
        Assert::oneOf($type, self::getTypes());

        $this->type = $type;
    }

    public function isEqual($type) {
        Assert::integer($type);

        return $this->type === $type;
    }

    public function getCurrent() {
        return $this->type;
    }

    public function setType($type) {
        return new self($type);
    }

    public function setCode($code) {
        Assert::oneOf($code, self::getCodes());
        $codes = array_flip(self::getCodes());
        return new self($codes[$code]);
    }

    public function hasWholesaleNone() {
        return $this->type === self::WHOLESALE_NONE;
    }

    public function hasWholesaleSmall() {
        return $this->type === self::WHOLESALE_1;
    }

    public function hasWholesaleMedium() {
        return $this->type === self::WHOLESALE_2;
    }

    public function hasWholesaleBig() {
        return $this->type == self::WHOLESALE_3;
    }

    public function hasWholesale4() {
        return $this->type === self::WHOLESALE_4;
    }


    public static function getTypes() {
        return [
            self::WHOLESALE_NONE,
            self::WHOLESALE_1,
            self::WHOLESALE_2,
            self::WHOLESALE_3,
            self::WHOLESALE_4,
        ];
    }

    public static function getCodes() {
        return [
            self::WHOLESALE_NONE => 'wholesale_none',
            self::WHOLESALE_1 => 'wholesale_small',
            self::WHOLESALE_2 => 'wholesale_middle',
            self::WHOLESALE_3 => 'wholesale_big',
            self::WHOLESALE_4 => 'wholesale_large',
        ];
    }

    public static function getLabels() {
        return [
            self::WHOLESALE_NONE => 'без скидки',
            self::WHOLESALE_1 => 'скидка по мелкому опту',
            self::WHOLESALE_2 => 'скидка по среднему опту',
            self::WHOLESALE_3 => 'скидка по крупному опту',
            self::WHOLESALE_4 => 'скидка по опт 4',
        ];
    }

    public function getLabel() {
        return self::getLabels()[$this->type];
    }

    public function getCode() {
        return self::getCodes()[$this->type];
    }
}