<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 27.07.2017
 * Time: 16:31
 */

namespace AppBundle\system\helpers;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style;
use Webmozart\Assert\Assert;

class ExcelExportHelper {

    private $_row = 1;
    private $sheet = null;
    private $xls = null;

    public function __construct($title)
    {
        $this->xls = new Spreadsheet();
        $this->sheet = $this->xls->getActiveSheet();
        $this->sheet->setTitle($title);
        $this->_addHeader();
    }


    public function setCell($values) {
        $this->_row += 1;
        $index = 0;
        foreach ($values as $value) {
            $cell = $this->sheet->getCellByColumnAndRow($index, $this->_row);
            if(is_callable($value['value']) || is_array($value['value'])) {
                call_user_func_array($value['value'], [
                    'row' => $this->_row,
                    'index' => $index,
                    'sheet' => $this->sheet
                ]);
            }
            else {
                $cell->setValue($value[ 'value' ]);
            }
            if(isset($value['styleCallback'])) {
                call_user_func_array($value['styleCallback'], ['style' => $cell->getStyle(), 'index' => $index, 'row' => $this->_row]);
            }
            $index++;
        }
    }

    protected function _addHeader()
    {
        $this->sheet->setCellValue('A1', 'Прайс-лист от '.date('d.m.Y'));
        $this->sheet->mergeCells('A1:H1');
        $this->_formatHeader($this->sheet->getStyle('A1'));

        $this->sheet->setCellValue('A2',
            'Компания "Мобилак" занимается оптовой продажей карт памяти для мобильных устройств, Usb накопителей, аксеcсуаров для мобильных телефонов, внешних жестких дисков.');
        $this->sheet->mergeCells('A2:H2');
        $this->_formatHeader($this->sheet->getStyle('A2'));

        $this->sheet->setCellValue('A3', 'Контакты: 8 (911) 922-2460; 8 (911) 922-2450');
        $this->sheet->mergeCells('A3:H3');
        $this->_formatHeader($this->sheet->getStyle('A3'));

        $this->sheet->setCellValue('A4', 'e-mail: email');
        $this->sheet->mergeCells('A4:H4');
        $this->_formatHeader($this->sheet->getStyle('A4'));

        $this->_row = 4;
    }

    protected function _formatHeader(Style $style) {
        $style->getFill()->setFillType(Style\Fill::FILL_SOLID);
        $style->getFill()->getStartColor()->setRGB('00C2E6');
        $style->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_CENTER);
        $style->getFont()->getColor()->setRGB('ffffff');
        /*        $style->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
                $style->getBorders()->getBottom()->getColor()->setRGB();*/
    }


    private function styleThCell(Style $style, $index) {
        $style->getAlignment()->setHorizontal(Style\Alignment::HORIZONTAL_CENTER);
        $style->getFont()->setBold(true);
        $this->sheet->getColumnDimensionByColumn($index)->setAutoSize(true);
    }

    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->_row;
    }

    /**
     * @return null|\PhpOffice\PhpSpreadsheet\Worksheet
     */
    public function getSheet()
    {
        return $this->sheet;
    }

    /**
     * @return null|Spreadsheet
     */
    public function getXls()
    {
        return $this->xls;
    }
}