<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.07.2017
 * Time: 18:59
 */

namespace AppBundle\system;


use AppBundle\services\ConfigManager;
use AppBundle\services\IConfigManager;
use Assert\Assert;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Currency\Context\CurrencyContextInterface;
use Sylius\Component\Currency\Context\CurrencyNotFoundException;

class DefaultCurrencyContext implements CurrencyContextInterface
{
    /**
     * @var CurrencyContextInterface
     */
    private $currencyContext;

    /**
     * @var ChannelContextInterface
     */
    private $channelContext;

    /**
     * @var ConfigManager
     */
    private $configManager;

    /**
     * @param CurrencyContextInterface $currencyContext
     * @param ChannelContextInterface $channelContext
     * @param IConfigManager $configManager
     */
    public function __construct(
        CurrencyContextInterface $currencyContext,
        ChannelContextInterface$channelContext,
        IConfigManager $configManager
    )
    {
        $this->currencyContext = $currencyContext;
        $this->channelContext = $channelContext;
        $this->configManager = $configManager;
    }

    public function getCurrencyCode(): string
    {
        /** @var ChannelInterface $channel */
        $channel = $this->channelContext->getChannel();

        $currencies = $channel->getCurrencies()->toArray();
        $codes = array_filter($currencies, function($currency) {
            return $this->configManager->getDefaultCurrencyCode() === $currency->getCode();
        });

        \Webmozart\Assert\Assert::count($codes, 1);
        $currency = array_shift($codes);

        return $currency->getCode();
    }

}