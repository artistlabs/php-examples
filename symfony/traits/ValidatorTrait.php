<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 24.07.2017
 * Time: 16:19
 */
namespace AppBundle\system\traits;
use AppBundle\exception\NotValidException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webmozart\Assert\Assert;

trait ValidatorTrait
{
    public function validate($obj, $group = 'sylius') {
        Assert::isInstanceOf($this->validator, ValidatorInterface::class, ValidatorInterface::class.' not found in trait');
        $errors = $this->validator->validate($obj, null, $group);
        if (count($errors) > 0) {
            throw new NotValidException('validate '.get_class($obj).' errors: '.(string)$errors);
        }
    }
}