/**
 * Created by vladimir on 10.04.2015.
 */
var GulpSSH = require('gulp-ssh');
var gulp = require('gulp');
var fs = require('fs');
var less = require('gulp-less'),
    rename = require('gulp-rename'),
    spritesmith = require('gulp.spritesmith'),
    path = require('path'),
    _ = require('lodash'),
    gexec = require('gulp-exec'),
    deploy = require('./deploy'),
    runSequence = require('run-sequence'),
    shell = require('gulp-shell'),
    gutil = require('gulp-util'),
    clean = require('gulp-clean'),
    argv = require('yargs').argv,
    webpackStream = require('webpack-stream');


var currentBranch = argv.branch || 'dev';
var webpack = webpackStream.webpack;
var IS_DEBUG = (process.env.LOGNAME === 'vagrant');

//////////////////
// DEPLOY TO TEST
//////////////////
gulp.task('test-sync', function () {
    return deploy.rsyncRun(deploy.settings.path.test);
});

gulp.task('test-shell', function () {
    return deploy.runShell(deploy.settings.path.test)
        .pipe(gulp.dest(deploy.settings.logPath));
});

//////////////////


////////////// branch dev
gulp.task('deploy-dev', function () {
    gutil.log(gutil.colors.green("\t\t\t--== Update dev version ==--"));
    return runSequence(
        'git',
        'npm',
        'reset-env',
        'bower',
        'composer',
        'less',
        'webpack-common',
        'chown',
        'chmod',
        'migrations',
        'unit-test',
        //'codecept-acceptance-test',
        'dev-test',
        'success'
    );

});
gulp.task('server:deploy-test', function () {
    gutil.log(gutil.colors.green("\t\t\t--== Update main master version ==--"));
    return runSequence(
        'git',
        'npm',
        'reset-env',
        'bower',
        'composer',
        'less',
        'chown',
        'chmod',
        'webpack-common',
        'assets',
        'migrations',
        'chown',
        'chmod',
        'main-test',
        'success'
    );
});

///////////// local dev
gulp.task('deploy-local', function () {
    currentBranch = 'dev';
    gutil.log(gutil.colors.green("\t\t\t--== Update local version ==--"));
    return runSequence('migrations', 'local-test');

});


///////////////////// tasks ////////////////
gulp.task('success', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== SUCCESS UPDATE ==--\n"));
});
gulp.task('git', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== GIT ==--\n"));
    return gulp.src('')
        .pipe(gexec('git reset HEAD --hard', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
        .pipe(gexec('git checkout ' + currentBranch, deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
        .pipe(gexec('git pull origin', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));

});

gulp.task('npm', function () {
    gutil.log(gutil.colors.green("\n\t\t\t--== NPM update ==--\n"));
    return gulp.src('')
        .pipe(gexec('npm update', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));
});

gulp.task('bower', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== BOWER update ==--\n"));
    return gulp.src('')
        .pipe(gexec('bower update --allow-root', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));

});

gulp.task('composer', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== COMPOSER update ==--\n"));
    return gulp.src('')
        .pipe(gexec('composer install --prefer-dist', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));

});

gulp.task('chown', function () {
    gutil.log(gutil.colors.green("--==CHOWN www-data==--"));
    return gulp.src('')
        .pipe(gexec('chown -R ' + deploy.settings.username + ':' + deploy.settings.serverGroup + ' .;', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));
});

gulp.task('chmod', function () {
    gutil.log(gutil.colors.green("--==CHMOD 775 ==--"));

    return gulp.src('')
        .pipe(gexec('chmod -R 775 *;', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));
});

gulp.task('migrations', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== MIGRATION run ==--\n"));
    return gulp.src('')
        .pipe(gexec('console/yii migrate --interactive=0', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec('tests/codeception/bin/yii migrate --interactive=0', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

gulp.task('assets', function () {
    gutil.log(gutil.colors.green("\n\t\t\t --== ASSETS run ==--\n"));
    return gulp.src('')
        .pipe(gexec('console/yii asset frontend/config/minify-template.php frontend/config/prod-asset.php', deploy.getGulpExecOptions(currentBranch)))
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)));
});

function done(error) {
    console.log(error);
    this.emit('end');
}

gulp.task('unit-test', function (done) {
    gutil.log(gutil.colors.green("\n\t\t\t --== UNIT TEST ==--\n"));

    return gulp.src('')
        .pipe(gexec('tests/codeception/bin/yii migrate --interactive=0'))
        .on('error', done)
        .pipe(gexec('codecept run unit -c tests/codeception/frontend/'))
        .on('error', done)
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

gulp.task('codecept-acceptance-test', function (done) {
    gutil.log(gutil.colors.green("\n\t\t\t --== ACCEPTANCE CODECEPTION TEST ==--\n"));

    return gulp.src('')
        .pipe(gexec('codecept run acceptance -c tests/codeception/frontend/'))
        .on('error', done)
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

gulp.task('dev-test', function (done) {
    gutil.log(gutil.colors.green("\n\t\t\t --== ACCEPTANCE TEST for dev run ==--\n"));

    return gulp.src('')
        .pipe(gexec('casperjs test ./tests/casperjs/dev/'))
        .on('error', done)
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

/**
 * тестирование test.taskrealtor.ru
 */
gulp.task('main-test', function (done) {
    gutil.log(gutil.colors.green("\n\t\t\t --== ACCEPTANCE TEST for master run ==--\n"));

    return gulp.src('')
        .pipe(gexec('casperjs test ./tests/casperjs/master/'))
        .on('error', done)
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

gulp.task('local-test', function (done) {
    gutil.log(gutil.colors.green("\n\t\t\t --== ACCEPTANCE TEST for local run ==--\n"));

    return gulp.src('')
        .pipe(gexec('casperjs test ./tests/casperjs/local/'))
        .on('error', done)
        .pipe(gexec.reporter(deploy.getGulpExecReportOptions(currentBranch)))
});

gulp.task('default', function () {
    console.log('--privateKey=pat-to-ssh_key  for deploy');
    console.log('dev-deploy - for deploy dev version too server');
    console.log("Все параметры в файле deploy/settings.json можно переопределить через аргументы.\n\tнапример: --noDryRun  укажет true для запуска в режиме копирования" +
    "\n\tБез этой опции реального копирования на сервер не будет");
});

gulp.task('watch', ['less', 'gensprite', 'watch-webpack'], function () {
    //следим за less
    gulp.watch([
        'frontend/web/css/less/*.less',
        'frontend/web/css/less/**/*.less',
        'backend/web/css/less/*.less',
        '!assets/**'
    ], ['less']).on('change', function (event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
    //следим за спрайтами
    gulp.watch(['frontend/web/img/icons/*.+(png|jpg|gif)', '!assets/**'], ['gensprite']).on('change', function (event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('less', function () {
    gulp.src([
        'frontend/web/css/less/*.less',
        'backend/web/css/less/*.less',
        '!frontend/web/css/less/partial/**',
        '!assets/**'])
        .pipe(rename(function (file) {
            //записываем в родителскую директорию
            file.dirname += '/../';
        }))
        .pipe(less()).on('error', function (event) {
            console.log(event);
            console.error.bind(console);
        })
        .pipe(gulp.dest(function (file) {
            return file.base;
        }));
});

gulp.task('gensprite', function () {
    var sprites = gulp.src(['frontend/web/img/icons/icons/*.*', '!assets/**'])
        .pipe(spritesmith({
            imgName: '../img/sprites.png',
            cssName: '_sprites.less',
            padding: 2,
            cssVarMap: function (sprite) {
                sprite.name = 'icon_' + sprite.name;
            }
        }));
    sprites.img.pipe(gulp.dest('./img/'));
    sprites.css.pipe(gulp.dest('./css/less/'));
});

gulp.task('delete-env', function() {
    "use strict";
    return gulp.src('.env', {read: false})
        .pipe(clean());
});

gulp.task('reset-env', ['delete-env'], function() {
    "use strict";
    var envFile = '.env.'+currentBranch;
    return gulp.src(envFile)
        .pipe(rename('.env'))
        .pipe(gulp.dest('.'));
});

//////WEBPACK
gulp.task('watch-webpack', ['webpack-common'], function() {
    //следим за js
    gulp.watch(['client/pages/**/*.js'], ['webpack-common']).on('change', function (event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('webpack-common', function() {
    /**
     * task используется только для разбиения старого файла site.js на составляющие и
     * написания новых,
     * зависимости прописываются с помощью yii assets
    */
    var options = {
        context: __dirname + '/client',
        entry: "./pages/common",
        output: {
            // Make sure to use [name] or [id] in output.filename
            //  when using multiple entry points
            filename: "common.js"
        },
        devtool: IS_DEBUG? 'source-map' : null,
        module: {
            loaders: [{
                test: /\.js$/,
                loader: 'babel',
                query: {
                    presets: ['es2015']
                }
            }]
        }
    };

    return gulp.src('client/pages/**/*.js')
        .pipe(webpackStream(options))
        .on('error', done)
        .pipe(gulp.dest('frontend/web/js/build'));
});
