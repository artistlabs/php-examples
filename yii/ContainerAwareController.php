<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.04.2017
 * Time: 11:08
 */

namespace common\core\system;


use common\core\contracts\ILogger;
use common\core\entity\user\contracts\IUserService;
use dekey\di\contracts\Container;
use dekey\di\mixins\ContainerAccess;
use yii\base\Module;
use yii\base\Response;
use yii\web\Controller;
use yii\web\Request;

class ContainerAwareController extends Controller
{
    use ContainerAccess;

    /**
     * @return Request
     */
    public function getRequest() {
        return $this->getContainer()->get('request');
    }

    /**
     * @return \yii\web\Response
     */
    public function getResponse() {
        return $this->getContainer()->get('response');
    }

    /**
     * @return ILogger
     */
    public function getLogger() {
        return $this->getContainer()->get(ILogger::class);
    }

    /**
     * get from Service Locator
     *
     * @param $key
     */
    public function get($key, $config = []) {
        return $this->getContainer()->get($key, [], $config);
    }

    /**
     * @return IUserService
     */
    public function getUserService() {
        return $this->get(IUserService::class);
    }
}