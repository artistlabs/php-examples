<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 09.06.2017
 * Time: 12:28
 */

namespace common\core\system;


use common\core\contracts\IAppService;
use yii\helpers\ArrayHelper;

class YiiAppService implements IAppService
{
    public function getCurrentLang()
    {
        return \Yii::$app->getCurrentLang();
    }

    public function getHasCurrentLang($lang)
    {
        return \Yii::$app->getHasCurrentLang($lang);
    }

    public function getConfigParams($key, $default = null)
    {
        return ArrayHelper::getValue(\Yii::$app->params, $key, $default);
    }
}