<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 09.06.2017
 * Time: 12:29
 */

namespace common\core\system;


use common\core\contracts\ITranslator;

class YiiTranslator implements ITranslator
{
    public function translate($category, $message, $params = [])
    {
        return \Yii::t($category, $message, $params);
    }

}