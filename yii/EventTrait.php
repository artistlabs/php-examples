<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 03.05.2017
 * Time: 11:57
 */

namespace common\core\system;


trait EventTrait
{
    private $events = [];

    protected function recordEvent($event)
    {
        $this->events[] = $event;
    }

    public function releaseEvents()
    {
        $events = $this->events;
        $this->events = [];
        return $events;
    }
}