<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.04.2017
 * Time: 13:51
 */

namespace common\core\system;


use cherif\tactician\Tactician;
use common\components\contracts\IConfigComponent;
use common\components\Mailer;
use common\core\contracts\IAppService;
use common\core\contracts\ILogger;
use common\core\contracts\IMailer;
use common\core\contracts\ISecurity;
use common\core\contracts\ISession;
use common\core\contracts\ITranslator;
use common\core\entity\agency\AgencyCommentsRatingRepository;
use common\core\entity\agency\AgencyCommentsRepository;
use common\core\entity\agency\AgencyRepository;
use common\core\entity\agency\AgencyService;
use common\core\entity\agency\command\AgencyCommentsRatingVoteCommand;
use common\core\entity\agency\command\AgencyCommentsRatingVoteCommandHandler;
use common\core\entity\agency\command\backend\ApproveChangesCommand;
use common\core\entity\agency\command\backend\ApproveChangesCommandHandler;
use common\core\entity\agency\command\BackendCreateNewAgencyCommand;
use common\core\entity\agency\command\BackendEditAgencyCommand;
use common\core\entity\agency\command\BackendEditAgencyCommandHandler;
use common\core\entity\agency\command\EditAgencyProfileCommand;
use common\core\entity\agency\command\EditAgencyProfileCommandHandler;
use common\core\entity\agency\command\partial\AgencyChangePassword;
use common\core\entity\agency\command\SignupAgencyFromExistUserCommand;
use common\core\entity\agency\contract\IAgencyCommentsRatingRepository;
use common\core\entity\agency\contract\IAgencyCommentsRepository;
use common\core\entity\agency\contract\IAgencyRepository;
use common\core\entity\agency\form\AgencyFormBuilder;
use common\core\entity\agency\form\BackendAgencyForm;
use common\core\entity\agency\form\ProfileAgencyForm;
use common\core\entity\user\command\ChangePasswordCommandHandler;
use common\core\entity\agency\command\SignupAgencyFromNewUserCommand;
use common\core\entity\agency\command\SignupAgencyCommandHandler;
use common\core\entity\agency\contract\IAgencyService;
use common\core\entity\agency\form\SignupAgencyForm;
use common\core\entity\systemEvent\contracts\ISystemEventRepository;
use common\core\entity\systemEvent\contracts\ISystemEventService;
use common\core\entity\systemEvent\SystemEventRepository;
use common\core\entity\systemEvent\SystemEventService;
use common\core\entity\user\command\ResetPasswordCommand;
use common\core\entity\user\command\ResetPasswordCommandHandler;
use common\core\entity\user\command\SignInConfirmCommand;
use common\core\entity\user\command\SigninConfirmCommandHandler;
use common\core\entity\user\contracts\IUserRepository;
use common\core\entity\user\contracts\IUserService;
use common\core\entity\user\form\ConfirmForm;
use common\core\entity\user\form\PasswordResetForm;
use common\core\entity\user\UserRepository;
use common\core\entity\user\UserService;
use common\core\keyStorage\contracts\IKeyStorageFactory;
use common\core\keyStorage\contracts\IKeyStorageService;
use common\core\system\dispatchers\DummyDispatcher;
use dekey\di\mixins\ContainerAccess;
use League\Tactician\CommandBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Plugins\LockingMiddleware;
use League\Tactician\Setup\QuickStart;
use ProxyManager\Factory\LazyLoadingValueHolderFactory;
use yii\base\BootstrapInterface;
use yii\di\Instance;
use yii\web\Application;

class Bootstrap implements BootstrapInterface
{
    use ContainerAccess;

    /**
     * @var Application
     */
    private $app;

    public function bootstrap($app)
    {
        \Yii::$container = new \dekey\di\Container();
        $this->app = $app;

        $this
            ->initBaseServices()
            ->initKeyStorage()
            ->initUserEntity()
            ->initSystemEventEntity()
            ->initAgency();
    }


    protected function initBaseServices()
    {
        $this->getContainer()->setSingleton('request', $this->app->request);
        $this->getContainer()->setSingleton('response', $this->app->response);
        $this->getContainer()->setSingleton(ILogger::class, Logger::class);
        $this->getContainer()->set('ICustomEvent', \common\components\Event::className());
        $this->getContainer()->set('common\components\contracts\IConfigComponent', function () {
            return $this->app->cnf;
        })
            ->setSingleton(ITranslator::class, YiiTranslator::class)
            ->setSingleton(IAppService::class, YiiAppService::class);


        $this->getContainer()->setSingleton(IMailer::class, Mailer::className(), [
            $this->app->frontendMailer,
            $this->app->backendMailer,
            $this->app->consoleMailer,
            Instance::of(IUserRepository::class),
            [
                'from' => getenv('MAILER_FROM'),
            ],
        ])
            ->set('user', $this->app->user)
            ->set(DummyDispatcher::class, DummyDispatcher::class)
            ->setSingleton(\common\core\system\Hydrator::class)
            ->setSingleton(ISession::class, YiiSession::class)
            ->setSingleton(ISecurity::class, YiiSecurity::class)
            ->setSingleton('commandBus', function () {
                return $this->initCommandBus();
            });
        $this->getContainer()->setSingleton('proxyManagerFactory', function() {
            $path = \Yii::$app->getRuntimePath().'/cache';
            if(!file_exists($path)) {
                mkdir($path, 0755);
            }

            $config = new \ProxyManager\Configuration();
            $config->setProxiesTargetDir($path);

            return new LazyLoadingValueHolderFactory($config);
        });

        return $this;
    }

    protected function initKeyStorage()
    {
        $container = $this->getContainer();
        $container->setSingleton(IKeyStorageFactory::class, \common\core\keyStorage\Factory::className());
        $container->set(IKeyStorageService::class, \common\core\keyStorage\Service::className());
        $container->set('common\core\keyStorage\contracts\IKeyStorageRepository',
            \common\core\keyStorage\Repository::className());

        return $this;
    }

    protected function initUserEntity()
    {
        $container = $this->getContainer();

        $container->setSingleton(IUserRepository::class, UserRepository::class, [
            Instance::of('proxyManagerFactory'),
        ]);
        $container->setSingleton(IUserService::class, UserService::class, [
            Instance::of(IUserRepository::class),
            Instance::of(IMailer::class),
            Instance::of('user'),
        ]);

        //reset password
        $container->set('core.user.form.resetPassword', PasswordResetForm::className());
        $container->set(ResetPasswordCommandHandler::class, ResetPasswordCommandHandler::class);

        //confirm auth
        $container->set('core.user.form.confirm', ConfirmForm::className());
        $container->set(SigninConfirmCommandHandler::class, SigninConfirmCommandHandler::class);

        $container->set(ChangePasswordCommandHandler::class, ChangePasswordCommandHandler::class);

        return $this;
    }

    protected function initSystemEventEntity()
    {
        $this->getContainer()
            ->setSingleton(ISystemEventRepository::class, SystemEventRepository::class)
            ->setSingleton(ISystemEventService::class, SystemEventService::class, [
                Instance::of(ISystemEventRepository::class),
                Instance::of(DummyDispatcher::class)
            ]);

        return $this;
    }

    protected function initAgency()
    {

        $this->getContainer()
            ->setSingleton(IAgencyService::class, AgencyService::class, [
                Instance::of(ITranslator::class),
                Instance::of(IConfigComponent::class),
            ])
            ->setSingleton(IAgencyRepository::class, AgencyRepository::class)
            ->setSingleton(IAgencyCommentsRepository::class, AgencyCommentsRepository::class)
            ->setSingleton(IAgencyCommentsRatingRepository::class, AgencyCommentsRatingRepository::class)
            ->set(SignupAgencyCommandHandler::class, SignupAgencyCommandHandler::class, [
                Instance::of(ITranslator::class),
                Instance::of(IUserService::class),
                Instance::of(IMailer::class),
            ])
            ->set(EditAgencyProfileCommandHandler::class, EditAgencyProfileCommandHandler::class, [
                Instance::of(IMailer::class),
                Instance::of(ITranslator::class),
                Instance::of(IUserRepository::class),
                Instance::of(ISession::class)
            ])
            ->set(BackendEditAgencyCommandHandler::class, BackendEditAgencyCommandHandler::class)
            ->set('core.agency.form.signup', SignupAgencyForm::className(), [
                Instance::of(IConfigComponent::class),
                Instance::of(IAppService::class),
                Instance::of(ITranslator::class),
                Instance::of(IUserService::class),
                Instance::of(IAgencyService::class)
            ])
            ->set(ApproveChangesCommandHandler::class, ApproveChangesCommandHandler::class, [
                Instance::of(IMailer::class),
                Instance::of(ITranslator::class)
            ])
            ->set(AgencyCommentsRatingVoteCommandHandler::class, AgencyCommentsRatingVoteCommandHandler::class)
            ->set('core.agency.form.profile', [AgencyFormBuilder::class, 'buildProfileForm'])
            ->set('core.agency.form.backend', [AgencyFormBuilder::class, 'buildBackendForm'])
            ->set('core.agency.form.backend__new-user', [AgencyFormBuilder::class, 'createBackendForm']);

        return $this;
    }

    protected function initCommandBus()
    {
        $handlerMiddleware = new CommandHandlerMiddleware(
            new ClassNameExtractor(),
            new InMemoryLocator([

                //user
                ResetPasswordCommand::class           => $this->getContainer()->get(ResetPasswordCommandHandler::class),
                SignInConfirmCommand::class => $this->getContainer()->get(SigninConfirmCommandHandler::class),

                //agency
                SignupAgencyFromNewUserCommand::class => $this->getContainer()->get(SignupAgencyCommandHandler::class),
                SignupAgencyFromExistUserCommand::class => $this->getContainer()->get(SignupAgencyCommandHandler::class),
                AgencyChangePassword::class => $this->getContainer()->get(ChangePasswordCommandHandler::class),
                EditAgencyProfileCommand::class => $this->getContainer()->get(EditAgencyProfileCommandHandler::class),
                BackendCreateNewAgencyCommand::class => $this->getContainer()->get(BackendEditAgencyCommandHandler::class),
                BackendEditAgencyCommand::class => $this->getContainer()->get(BackendEditAgencyCommandHandler::class),
                ApproveChangesCommand::class => $this->getContainer()->get(ApproveChangesCommandHandler::class),
                AgencyCommentsRatingVoteCommand::class => $this->getContainer()->get(AgencyCommentsRatingVoteCommandHandler::class)
            ]),
            new HandleInflector()
        );

        $lockingMiddleware = new LockingMiddleware();
        $transactionMiddleware = new TransactionCommandMiddleware();

        return new CommandBus([
            $lockingMiddleware,
            $transactionMiddleware,
            $handlerMiddleware
        ]);
    }
}