<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 14.07.2017
 * Time: 11:24
 */

namespace common\core\system;


use common\core\contracts\ISecurity;

class YiiSecurity implements ISecurity
{
    public function generatePasswordHash($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    public function generateRandomString($length = 32)
    {
        return \Yii::$app->security->generateRandomString($length);
    }

}