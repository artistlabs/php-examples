<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.06.2017
 * Time: 12:05
 */

namespace common\core\system;


use common\core\exceptions\ModelValidateException;
use common\core\exceptions\NotSaveException;
use yii\db\ActiveRecord;

trait ValidateAndSaveActiveRecordTrait
{
    private function validateAndSave(ActiveRecord $model)
    {
        if (!$model->validate()) {
            throw new ModelValidateException($model);
        }

        if (!$model->save(false)) {
            throw new NotSaveException('error save model - '.get_class($model));
        }
    }
}