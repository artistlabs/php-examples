<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 26.06.2017
 * Time: 12:37
 */

namespace common\core\system;


use common\core\contracts\ISession;

final class YiiSession implements ISession
{
    public function setFlash($key, $value = true, $removeAfterAccess = true)
    {
        \Yii::$app->session->setFlash($key, $value, $removeAfterAccess);
    }

    public function getFlash($key, $defaultValue = null, $delete = false)
    {
        \Yii::$app->session->getFlash($key, $defaultValue, $delete);
    }
}