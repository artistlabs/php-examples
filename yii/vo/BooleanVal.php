<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25.07.2017
 * Time: 13:29
 */

namespace common\core\system\vo;


use Webmozart\Assert\Assert;

class BooleanVal
{
    private $value;

    public function __construct($value)
    {
        if($value === 'true') {
            $value = true;
        }

        if($value === 'false') {
            $value = false;
        }



        $value = boolval($value);
        Assert::boolean($value);
        $this->value = $value;
    }

    public function isTrue() {
        return $this->value === true;
    }

    public function isFalse() {
        return $this->value === false;
    }

    public function getValue() {
        return $this->value;
    }

    public function setValue($value) {
        return new self($value);
    }
}