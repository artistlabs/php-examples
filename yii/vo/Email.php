<?php

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 14.06.2017
 * Time: 15:33
 */
namespace common\core\system\vo;

class Email
{
    private $email;

    public function __construct($email)
    {
        \Webmozart\Assert\Assert::string($email);
        $this->email = $email;
    }

    public function setValue($email) {
        return new self($email);
    }

    public function getValue() {
        return $this->email;
    }

    public function isEqual($email) {
        return $this->email === $email;
    }
}