<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 21.06.2017
 * Time: 11:55
 */

namespace common\core\system\vo;


use Webmozart\Assert\Assert;

class Languages
{
    const LANG_RU='ru';
    const LANG_ru_RU='ru-RU';
    const LANG_EN='en';
    const LANG_en_US='en-US';

    private $langs;

    public function __construct(array $langs)
    {
        Assert::isArray($langs);
        Assert::allOneOf($langs, self::getLanguages());

        $this->langs = $langs;
    }

    /**
     * @param $lang
     * @return bool
     */
    public function hasLanguage($lang) {
        return in_array($lang, $this->langs);
    }

    /**
     * @return bool
     */
    public function hasEnLanguage() {
        return $this->hasLanguage(self::LANG_EN) || $this->hasLanguage(self::LANG_en_US);
    }

    public function hasRuLanguage() {
        return $this->hasLanguage(self::LANG_RU) || $this->hasLanguage(self::LANG_ru_RU);
    }

    /**
     * @return array
     */
    public function getLangs()
    {
        return $this->langs;
    }

    /**
     * @param array $langs
     */
    public function setLangs($langs)
    {
        return new self($langs);
    }



    public static function getLanguages() {
        return [
            self::LANG_RU,
            self::LANG_ru_RU,
            self::LANG_EN,
            self::LANG_en_US
        ];
    }
}