<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 16.06.2017
 * Time: 15:50
 */

namespace common\core\system\vo;


use Webmozart\Assert\Assert;

class SlugField
{
    private $value;

    public function __construct($string)
    {
        Assert::string($string);
        $this->value = \URLify::filter($string);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        return new self($value);
    }


}