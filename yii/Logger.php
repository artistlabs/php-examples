<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 27.04.2017
 * Time: 11:39
 */

namespace common\core\system;


use common\core\contracts\ILogger;
use common\core\entity\systemEvent\contracts\ISystemEventService;

class Logger implements ILogger
{
    private $eventService;

    public function __construct(ISystemEventService $eventService)
    {
        $this->eventService = $eventService;
    }

    public function warning($message, $category)
    {
        \Yii::warning($message, $category);
    }

    public function error($message, $category, $stackTrace = '')
    {
        \Yii::error($message."\n".$stackTrace, $category);
    }

    public function info($message, $category)
    {
        \Yii::info($message, $category);
    }

    public function trace($message, $category)
    {
        \Yii::trace($message, $category);
    }

    public function systemEventLog($application, $category, $name, $data = [], $userId = null)
    {
        $this->eventService->create($application, $category, $name, $data, $userId);
    }

}