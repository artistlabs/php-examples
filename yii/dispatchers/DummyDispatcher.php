<?php

namespace common\core\system\dispatchers;

use common\core\contracts\IEventDispatcher;
use common\core\contracts\ILogger;

/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 04.05.2017
 * Time: 10:26
 */
class DummyDispatcher implements IEventDispatcher
{

    public function dispatch(array $events)
    {
        foreach ($events as $event) {
            \Yii::info('Dispatch event '. get_class($event), 'dummy-dispatcher');
        }
    }

}