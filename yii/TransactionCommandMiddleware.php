<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 13.06.2017
 * Time: 13:35
 */

namespace common\core\system;


use common\core\contracts\ICommand;
use League\Tactician\Middleware;
use Webmozart\Assert\Assert;

class TransactionCommandMiddleware implements Middleware
{
    /**
     * @param ICommand $command
     * @param callable $next
     * @return mixed
     * @throws \Exception
     */
    public function execute($command, callable $next)
    {
        Assert::isInstanceOf($command, ICommand::class);
        if ($command->isUsedTransaction() && \Yii::$app->db->transaction === null) {
            $tr = \Yii::$app->db->beginTransaction();
            try {
                $returnValue = $next($command);
                $tr->commit();
            } catch (\Exception $e) {
                $tr->rollBack();
                throw $e;
            }
        } else {
            $returnValue = $next($command);
        }

        return $returnValue;
    }

}